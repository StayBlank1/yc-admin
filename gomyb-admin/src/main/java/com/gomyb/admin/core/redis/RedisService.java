package com.gomyb.admin.core.redis;

import java.util.Set;

public interface RedisService {

    void set(String key, String value,int time);

    void set(String key, String value);

    String get(String key);

    void incr(String key);

    void del(String key);

    Set<String> keys(String pattern);

    Set<String> values(String pattern);

    int keySize(String pattern);

    void delAll(String pattern);

    Object getObj(String key);

    void setObj(String key, Object value);
}
