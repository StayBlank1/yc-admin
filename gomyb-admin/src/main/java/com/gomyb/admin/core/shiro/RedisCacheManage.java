package com.gomyb.admin.core.shiro;

import com.gomyb.admin.core.redis.RedisService;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/3/31 1:09
 * @Description: 权限缓存配置器
 */
public class RedisCacheManage implements CacheManager{

    protected RedisService redisService;

    public RedisCacheManage(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        return new RedisCache<K,V>(redisService);
    }
}
