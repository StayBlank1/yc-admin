package com.gomyb.admin.core.shiro.util;

import lombok.Data;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/3 23:35
 * @Description:
 */
@Data
public class PWD {

    private String password;

    private String salt;

}
