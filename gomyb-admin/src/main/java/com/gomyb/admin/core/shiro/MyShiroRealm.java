package com.gomyb.admin.core.shiro;

import com.gomyb.admin.module.user.model.SysUser;
import com.gomyb.admin.module.user.service.SysUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</b> HeC<br/>
 * @createTime 2018/3/30 23:52
 * @Description:
 */
public class MyShiroRealm extends AuthorizingRealm {

    @Autowired
    private SysUserService sysUserService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        Object primaryPrincipal = principals.getPrimaryPrincipal();
        /*for(AppRole role:appUser.getRoleList()){
            //添加角色
            authorizationInfo.addRole(role.getRoleKey());
            for(AppFn p:role.getAppFnList()){
                //添加拥有的权限
                authorizationInfo.addStringPermission(p.getFnKey());
            }
        }*/
        authorizationInfo.addRole("admin");
        authorizationInfo.addRole("admin1");
        authorizationInfo.addRole("admin2");
        authorizationInfo.addStringPermission("per1");
        authorizationInfo.addStringPermission("per2");
        authorizationInfo.addStringPermission("per3");
        authorizationInfo.addStringPermission("per4");
        authorizationInfo.addStringPermission("/user/info");
        authorizationInfo.addStringPermission("/getRoles");
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //UsernamePasswordToken对象用来存放提交的登录信息
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        SysUser user = sysUserService.getByUsername(token.getUsername());
        //用户是否存在
        if (user == null) {
            throw new UnknownAccountException();
        }
        //是否激活
        if (user.getStatus().equals(0)) {
            throw new DisabledAccountException();
        }
        //若存在，将此用户存放到登录认证info中，无需自己做密码对比Shiro会为我们进行密码对比校验
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getSalt());
        return new SimpleAuthenticationInfo(
                user.getUsername(), //用户名
                user.getPassword(), //密码
                credentialsSalt,//salt
                getName()  //realm name
        );
    }

}

