package com.gomyb.admin.core.redis;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/24 13:59
 * @Description:
 */
@ConfigurationProperties(prefix = "redis")
@Configuration
@Getter
@Setter
public class RedisConfig {

    private String host = "127.0.0.1";

    private String password = "";

    private Integer port = 6379;

    private Integer timeout = 1000;

    @Bean
    public JedisPool redisPoolFactory() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        if(StringUtils.isNotEmpty(password)){
           return new JedisPool(jedisPoolConfig, host, port, timeout,password);
        }else{
           return new JedisPool(jedisPoolConfig, host, port, timeout);
        }
    }

}