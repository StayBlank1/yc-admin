package com.gomyb.admin.core.redis;

import gomyb.model.util.SerializeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/30 10:32
 * @Description:
 */
@Service("redisService")
public class RedisServiceImpl implements RedisService {

    @Autowired
    private JedisPool jedisPool;

    @Override
    public void set(String key, String value,int time){
        try(Jedis jedis = jedisPool.getResource()){
            jedis.set(key, value);
            jedis.expire(key,time);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void set(String key, String value){
        try(Jedis jedis = jedisPool.getResource()){
            jedis.set(key, value);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String get(String key) {
        try(Jedis jedis = jedisPool.getResource()){
            return jedis.get(key);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void incr(String key) {
        try(Jedis jedis = jedisPool.getResource()){
            jedis.incr(key);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void del(String key) {
        try(Jedis jedis = jedisPool.getResource()){
            jedis.del(key);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Set<String> keys(String pattern) {
        try(Jedis jedis = jedisPool.getResource()){
            return jedis.keys(pattern);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<String> values(String pattern) {
        try(Jedis jedis = jedisPool.getResource()){
            Set<String> keys = jedis.keys(pattern);
            Set<String> set = new HashSet<String>();
            for (String key : keys) {
                set.add(jedis.get(key));
            }
            return set;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int keySize(String pattern) {
        try(Jedis jedis = jedisPool.getResource()){
            return jedis.keys(pattern).size();
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void delAll(String pattern) {
        try(Jedis jedis = jedisPool.getResource()){
            Set<String> keys = jedis.keys(pattern);
            for (String key : keys) {
                jedis.del(key);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Object getObj(String key) {
        try(Jedis jedis = jedisPool.getResource()){
            return SerializeUtil.unserialize(jedis.get(key.getBytes()));
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setObj(String key, Object value) {
        try(Jedis jedis = jedisPool.getResource()){
            jedis.set(key.getBytes(), SerializeUtil.serialize(value));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}