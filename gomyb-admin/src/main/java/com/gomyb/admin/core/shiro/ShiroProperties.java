package com.gomyb.admin.core.shiro;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/30 10:29
 * @Description:
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "shiro")
public class ShiroProperties {

    /**
     * 权限过滤链
     */
    private Map<String,String> filterUrl;

    /**
     * 登录url
     */
    private String loginUrl;

    /**
     * 成功url
     */
    private String successUrl;

    /**
     * 未授权url
     */
    private String unauthorizedUrl;

    /**
     * 登录密码错误限制次数
     */
    private Integer retryMax;

}