package com.gomyb.admin.core.shiro.util;

import com.gomyb.admin.common.sys.SpringContextUtil;
import com.gomyb.admin.core.redis.RedisService;
import com.gomyb.admin.core.shiro.RedisCache;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.Subject;

import java.util.Set;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</b> HeC<br/>
 * @createTime 2018/11/4 0:45
 * @Description:
 */
public class ShiroUtil {

    public static final String SHIRO_CACHE_PREFIX = "shiro:function:";

    private static RedisService redisService = (RedisService) SpringContextUtil.getBean("redisService");

    public static Set<String> getPermissions() {
        Subject subject = SecurityUtils.getSubject();
        if (null != subject) {
            Object username = subject.getPrincipal();
            SimpleAuthorizationInfo simpleAuthorizationInfo = (SimpleAuthorizationInfo) redisService.getObj(SHIRO_CACHE_PREFIX + username);
            if (null != simpleAuthorizationInfo) {
                return simpleAuthorizationInfo.getStringPermissions();
            }
        }
        return null;
    }

    public static Set<String> getRoles() {
        Subject subject = SecurityUtils.getSubject();
        if (null != subject) {
            Object username = subject.getPrincipal();
            SimpleAuthorizationInfo simpleAuthorizationInfo = (SimpleAuthorizationInfo) redisService.getObj(SHIRO_CACHE_PREFIX + username);
            if (null != simpleAuthorizationInfo) {
                return simpleAuthorizationInfo.getRoles();
            }
        }
        return null;
    }

}
