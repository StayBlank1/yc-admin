package com.gomyb.admin.web.base;

import com.gomyb.admin.web.page.PageParam;
import org.springframework.data.domain.Page;

import java.io.Serializable;

public interface BaseService<T, ID extends Serializable> {

    T get(ID id);

    Page<T> findPage(PageParam pageParam);

    T addObj(T obj);

}
