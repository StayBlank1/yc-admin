package com.gomyb.admin.web.base;

import cn.hutool.core.bean.BeanUtil;
import com.gomyb.admin.web.page.PageParam;
import com.gomyb.admin.web.page.PageResult;
import gomyb.model.result.JsonResult;
import gomyb.model.result.Result;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/29 21:09
 * @Description:
 */
public abstract class BaseController<T,ID extends Serializable> {

    protected BaseService<T, ID> service;

    protected Result get(ID id){
        if(null!=id) {
            T po = service.get(id);
            if (po != null) {
                return JsonResult.OK(po);
            } else {
                return JsonResult.NO("没有查询到数据");
            }
        }else{
            return JsonResult.NO("ID不能为空");
        }
    }

    protected T see(ID id){
        if(null!=id) {
            T po = service.get(id);
            if (po != null) {
                return po;
            }
        }
        return null;
    }

    /**
     * 把浏览器参数转化放到Map集合中
     */
    protected Map<String, Object> getParam(HttpServletRequest request) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        String method = request.getMethod();
        Enumeration<?> keys = request.getParameterNames();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            if (key != null) {
                if (key instanceof String) {
                    String value = request.getParameter(key.toString());
                    if ("GET".equals(method)) {
                        //前台encodeURIComponent('我们');转码后到后台还是ISO-8859-1，所以还需要转码
                        value = new String(value.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                    }
                    paramMap.put(key.toString(), value);
                }
            }
        }
        return paramMap;
    }

    /**
     * 分页查询
     */
    protected PageResult<T> findPage(HttpServletRequest request){
        Map<String, Object> param = getParam(request);
        PageParam pageParam;
        if(param!=null&&param.size()>0){
            pageParam = BeanUtil.mapToBean(param, PageParam.class,false);
        }else{
            pageParam = new PageParam();
        }
        Page<T> data = service.findPage(pageParam);
        if(null!=data&&data.getSize()>0){
            return new PageResult<T>(true,data.getTotalElements(),data.getContent());
        }
        return new PageResult<T>(false);

    }

    /**
     * 添加一个对象
     */
    protected Result save(T obj){
        try {
            obj = service.addObj(obj);
            if(obj!=null){
                return JsonResult.OK();
            }else{
                return JsonResult.NO();
            }
        }catch (Exception e){
            e.printStackTrace();
            return JsonResult.NO(e.getMessage());
        }
    }

    /**
     * 更新
     */
    protected Result update(T t){
        return save(t);
    }
}
