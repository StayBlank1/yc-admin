package com.gomyb.admin.web.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/29 19:53
 * @Description:
 */
@Getter
@Setter
@MappedSuperclass
public class BaseModel implements Serializable {

    @Column(name = "create_time")
    @JsonIgnore
    private Date createTime;

    @Column(name = "update_time")
    @JsonIgnore
    private Date updateTime;


}