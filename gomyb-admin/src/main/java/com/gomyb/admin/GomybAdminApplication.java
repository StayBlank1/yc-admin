package com.gomyb.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GomybAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(GomybAdminApplication.class, args);
    }
}
