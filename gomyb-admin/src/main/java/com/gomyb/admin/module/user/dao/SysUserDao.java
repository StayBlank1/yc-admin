package com.gomyb.admin.module.user.dao;

import com.gomyb.admin.module.user.model.SysUser;
import com.gomyb.admin.web.base.BaseDao;

/**
 * @author Hec
 */
public interface SysUserDao extends BaseDao<SysUser,Long> {

    SysUser getByUsername(String username);
}
