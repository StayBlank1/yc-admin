package com.gomyb.admin.module.user.service.impl;

import com.gomyb.admin.module.user.dao.SysUserDao;
import com.gomyb.admin.module.user.model.SysUser;
import com.gomyb.admin.module.user.service.SysUserService;
import com.gomyb.admin.web.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/29 20:02
 * @Description:
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser,Long> implements SysUserService{

    @Autowired
    private void setDao(SysUserDao dao){
        super.dao = dao;
    }

    @Override
    public SysUser getByUsername(String username) {
        return ((SysUserDao)dao).getByUsername(username);
    }
}