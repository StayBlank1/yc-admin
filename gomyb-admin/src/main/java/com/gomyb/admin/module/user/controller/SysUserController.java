package com.gomyb.admin.module.user.controller;

import com.alibaba.fastjson.JSON;
import com.gomyb.admin.core.redis.RedisService;
import com.gomyb.admin.core.shiro.util.PWD;
import com.gomyb.admin.core.shiro.util.PwdUtil;
import com.gomyb.admin.module.user.model.SysUser;
import com.gomyb.admin.module.user.service.SysUserService;
import com.gomyb.admin.web.base.BaseController;
import gomyb.model.result.Result;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/29 20:01
 * @Description:
 */
@RestController
@Log4j2
@RequestMapping("/sysUser")
public class SysUserController extends BaseController<SysUser,Long>{

    @Autowired
    private void setService(SysUserService sysUserService) {
        super.service = sysUserService;
    }

    @PostMapping("/get/{id}")
    public Result get(@PathVariable("id") Long id){
        return super.get(id);
    }

    @PostMapping("/save")
    public Result save(SysUser user){
        PWD pwd = PwdUtil.encryptPwd(user.getPassword());
        user.setPassword(pwd.getPassword());
        user.setSalt(pwd.getSalt());
        return super.save(user);
    }


}