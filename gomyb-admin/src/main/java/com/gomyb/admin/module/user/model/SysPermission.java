package com.gomyb.admin.module.user.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/4 15:59
 * @Description:
 */
@Entity
@Table(name = "sys_permission")
@Getter
@Setter
@DynamicUpdate
@DynamicInsert
public class SysPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "permission_url")
    private String permissionUrl;

    @Column(name = "permission_name")
    private String permissionName;

    @Column(name = "description")
    private String description;
}
