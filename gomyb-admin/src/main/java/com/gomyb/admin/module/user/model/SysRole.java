package com.gomyb.admin.module.user.model;

import com.gomyb.admin.web.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/4 15:52
 * @Description:
 */
@Entity
@Table(name = "sys_role")
@Getter
@Setter
@DynamicUpdate
@DynamicInsert
public class SysRole extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "description")
    private String description;

    @Transient
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "sys_role_permission",joinColumns = @JoinColumn(name = "role_id"),inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private List<SysPermission> permissionList;

}
