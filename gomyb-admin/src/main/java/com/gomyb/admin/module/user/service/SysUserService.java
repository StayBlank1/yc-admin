package com.gomyb.admin.module.user.service;

import com.gomyb.admin.module.user.model.SysUser;
import com.gomyb.admin.web.base.BaseService;

/**
 * @author Hec
 */
public interface SysUserService extends BaseService<SysUser,Long> {

    SysUser getByUsername(String username);
}
