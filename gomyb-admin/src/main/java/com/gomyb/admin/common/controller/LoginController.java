package com.gomyb.admin.common.controller;

import com.gomyb.admin.common.annotation.SysLog;
import com.gomyb.admin.common.model.UserInfo;
import com.gomyb.admin.core.shiro.util.ShiroUtil;
import com.gomyb.admin.module.user.model.SysUser;
import gomyb.model.result.JsonResult;
import gomyb.model.result.Result;
import lombok.extern.log4j.Log4j2;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</       b> HeC<br/>
 * @createTime 2018/11/3 22:28
 * @Description: 登录
 */
@RestController
@Log4j2
public class LoginController {


    /**
     * 登录校验
     */
    @SysLog("登录")
    @PostMapping("/login")
    public Result login(SysUser user, @RequestParam(defaultValue = "false") Boolean rememberMe) {
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
        //是否记住用户
        if (rememberMe) {
            token.setRememberMe(true);
        }
        //获得当前subject
        Subject currentUser = SecurityUtils.getSubject();
        //当前用户
        String username = user.getUsername();
        String msg = "登录成功";
        try {
            //登录校验
            currentUser.login(token);
        } catch (UnknownAccountException uae) {
            msg = "账户不存在";
        } catch (IncorrectCredentialsException ice) {
            msg = "密码不正确";
        } catch (DisabledAccountException lae) {
            msg = "账户未激活";
        } catch (ExcessiveAttemptsException eae) {
            msg = "用户名或密码错误次数过多";
        } catch (AuthenticationException ae) {
            // 通过处理Shiro的运行时AuthenticationException就可以控制用户登录失败或密码错误时的情景
            ae.printStackTrace();
            msg = "用户名或密码不正确";
        } catch (Exception e) {
            msg = e.getMessage();
            log.error(this.getClass(), e);
        }
        //验证登录状态
        if (currentUser.isAuthenticated()) {
            log.info("用户：{}  登录成功", username);
            currentUser.hasRole("");
            Map<String, Object> map = new HashMap<>(2);
            map.put("token",currentUser.getSession().getId());
            return JsonResult.OK(msg, map);
        } else {
            token.clear();
            return JsonResult.NO(msg);
        }
    }

    /**
     * 登陆状态验证
     */
    @SysLog("登录状态校验")
    @PostMapping("/loginAuth")
    public Result loginAuth() {
        if (SecurityUtils.getSubject().isAuthenticated()) {
            return JsonResult.OK();
        } else {
            return JsonResult.NO();
        }
    }

    /**
     * 用户登出
     */
    @PostMapping(value = "/logout")
    public Result logout() {
        SecurityUtils.getSubject().logout();
        return JsonResult.OK();
    }

    /**
     * 获得角色和权限
     */
    @PostMapping(value = "/getRoles")
    public Result getRoles() {
        Set<String> roles = ShiroUtil.getRoles();
        Set<String> permissions = ShiroUtil.getPermissions();
        if (null != roles && null != permissions) {
            Map<String, Object> map = new HashMap<>(2);
            map.put("roles", roles);
            map.put("permissions", permissions);
            return JsonResult.OK(map);
        }
        return JsonResult.NO();
    }

    /**
     * 用户信息
     */
    @PostMapping("/user/info")
    public Result getUserInfo() {
        if(SecurityUtils.getSubject().isAuthenticated()) {
            Set<String> roles = ShiroUtil.getRoles();
            UserInfo userInfo = new UserInfo();
            userInfo.setRoles(roles);
            userInfo.setName(SecurityUtils.getSubject().getPrincipal() + "");
            return JsonResult.OK(userInfo);
        }
        return JsonResult.NO();
    }

}
