package com.gomyb.admin.common.filter;

import com.alibaba.fastjson.JSON;
import com.gomyb.admin.core.shiro.util.ShiroUtil;
import gomyb.model.result.JsonResult;
import gomyb.model.result.ResultCode;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/4 16:15
 * @Description:
 */
@Service("myauthc")
public class CustomRolesAccessControlFilter extends AccessControlFilter {

    @Value("${shiro.login_url}")
    private String loginUrl;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse rep, Object mappedValue) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String requestURI = request.getRequestURI();
        if (loginUrl.equals(requestURI)) {
            return true;
        }
        if (request.getMethod().toUpperCase().equals("OPTIONS")) {
            return true;
        }
        Subject subject = SecurityUtils.getSubject();
        //url校验
        if (subject.isAuthenticated()) {
            Set<String> permissions = ShiroUtil.getPermissions();
            if (null != permissions && permissions.size() > 0 && permissions.contains(request.getRequestURI())) {
                return true;
            }
        }
        HttpServletResponse response = (HttpServletResponse) rep;
        response.setContentType("application/json; charset=utf-8");
        response.setCharacterEncoding("UTF-8");
        // 指定允许其他域名访问
        response.setHeader("Access-Control-Allow-Origin", "*");
        // 响应类型
        response.setHeader("Access-Control-Allow-Methods", "POST,GET");
        // 响应头设置
        response.setHeader("Access-Control-Allow-Headers", "token,Content-Type,Access-Control-Allow-Origin,Access-Control-Allow-Methods,Access-Control-Max-Age,authorization");
        try {
            response.getWriter().write(JSON.toJSONString(JsonResult.NO(ResultCode.NO_PERMISSION)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse response) throws Exception {
        return false;
    }
}
