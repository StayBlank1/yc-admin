package com.gomyb.admin.common.sys;

import com.gomyb.admin.core.redis.RedisService;
import com.gomyb.admin.core.shiro.util.ShiroUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/4 1:11
 * @Description:
 */
@Component
public class SysInitializing implements InitializingBean {

    @Autowired
    private RedisService redisService;

    @Override
    public void afterPropertiesSet() throws Exception {
        // 初始化权限缓存
        redisService.delAll(ShiroUtil.SHIRO_CACHE_PREFIX+"*");
    }
}
