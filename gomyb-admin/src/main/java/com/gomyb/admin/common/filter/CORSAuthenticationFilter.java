package com.gomyb.admin.common.filter;

import com.alibaba.fastjson.JSON;
import com.gomyb.admin.core.shiro.util.ShiroUtil;
import gomyb.model.result.JsonResult;
import gomyb.model.result.ResultCode;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</       b> HeC<br/>
 * @createTime 2018/11/4 23:15
 * @Description:
 */
//@Service("myauthc")
public class CORSAuthenticationFilter extends FormAuthenticationFilter {

    @Value("${shiro.login_url}")
    private String loginUrl;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse rep, Object mappedValue) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getMethod().toUpperCase().equals("OPTIONS")) {
            return true;
        }
        String requestURI = request.getRequestURI();
        if (loginUrl.equals(requestURI)) {
            return true;
        }
        if (super.isAccessAllowed(servletRequest, rep, mappedValue)) {
            Subject subject = SecurityUtils.getSubject();
            //url校验
            if (subject.isAuthenticated()) {
                Set<String> permissions = ShiroUtil.getPermissions();
                if (null != permissions && permissions.size() > 0 && permissions.contains(request.getRequestURI())) {
                    return true;
                }
            }
            HttpServletResponse response = (HttpServletResponse) rep;
            response.setContentType("application/json; charset=utf-8");
            response.setCharacterEncoding("UTF-8");
            // 指定允许其他域名访问
            response.setHeader("Access-Control-Allow-Origin", "*");
            // 响应类型
            response.setHeader("Access-Control-Allow-Methods", "POST,GET");
            // 响应头设置
            response.setHeader("Access-Control-Allow-Headers", "token,Content-Type,Access-Control-Allow-Origin,Access-Control-Allow-Methods,Access-Control-Max-Age,authorization");
            try (PrintWriter writer = response.getWriter()) {
                writer.write(JSON.toJSONString(JsonResult.NO(ResultCode.NO_PERMISSION)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletResponse res = (HttpServletResponse) response;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setStatus(HttpServletResponse.SC_OK);
        res.setCharacterEncoding("UTF-8");
        try (PrintWriter writer = res.getWriter()) {
            writer.write(JSON.toJSONString(JsonResult.NO(ResultCode.NO_LOGIN)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
