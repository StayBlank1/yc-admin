package com.gomyb.admin.common.model;

import lombok.Data;

import java.util.Set;

/**
 * @author <a href="mailto:18335553083@163.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/11/4 19:22
 * @Description:
 */
@Data
public class UserInfo {

    //角色名
    private Set<String> roles;

    //用户名
    private String name;

    //头像
    private String avatar;

    //简介
    private String introduction;

}
