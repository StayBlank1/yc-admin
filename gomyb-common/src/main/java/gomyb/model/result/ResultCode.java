package gomyb.model.result;

import lombok.Getter;

@Getter
public enum ResultCode {
    NO_PERMISSION(1001,"没有权限"),
    NO_LOGIN(1002,"没有登录"),
    ;

    ResultCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    private Integer code;

    private String description;

}
