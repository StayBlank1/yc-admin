package gomyb.model.result;

import lombok.Getter;
import lombok.Setter;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/25 20:28
 * @Description:
 */
@Getter
@Setter
public class NOResult extends Result {

    private boolean success = false;

    protected String message;

    protected Object data;

    protected Integer code;

    public NOResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public NOResult setData(Object data) {
        this.data = data;
        return this;
    }

    public NOResult setCode(Integer code) {
        this.code = code;
        return this;
    }
}