package gomyb.model.result;

import lombok.Getter;
import lombok.Setter;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</ b> HeC<br/>
 * @createTime 2018/10/25 20:27
 * @Description:
 */
@Getter
@Setter
public class OKResult extends Result {

    private boolean success = true;

    protected String message;

    protected Object data;

    protected Integer code;

    public OKResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public OKResult setData(Object data) {
        this.data = data;
        return this;
    }

    public OKResult setCode(Integer code) {
        this.code = code;
        return this;
    }

}