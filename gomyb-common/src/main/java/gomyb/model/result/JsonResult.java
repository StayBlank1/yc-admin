package gomyb.model.result;

/**
 * @author <a href="mailto:HelloHeSir@gmail.com">Mr_He</a>
 * @Copyright (c)</b> HeC<br/>
 * @createTime 2018/10/25 20:23
 * @Description:
 */
public class JsonResult {

    public static OKResult OK() {
        return new OKResult();
    }

    public static OKResult OK(ResultCode resultCode){
        return new OKResult().setCode(resultCode.getCode()).setMessage(resultCode.getDescription());
    }

    public static OKResult OK(String message) {
        return new OKResult().setMessage(message);
    }

    public static OKResult OK(Object data) {
        return new OKResult().setData(data);
    }

    public static OKResult OK(String message, Object data) {
        return new OKResult().setData(data).setMessage(message);
    }

    public static OKResult OK(Object data, String message) {
        return new OKResult().setData(data).setMessage(message);
    }

    public static NOResult NO() {
        return new NOResult();
    }

    public static NOResult NO(ResultCode resultCode) {
        return new NOResult().setCode(resultCode.getCode()).setMessage(resultCode.getDescription());
    }

    public static NOResult NO(String message) {
        return new NOResult().setMessage(message);
    }

    public static NOResult NO(Object data) {
        return new NOResult().setData(data);
    }

    public static NOResult NO(String message, Object data) {
        return new NOResult().setData(data).setMessage(message);
    }

    public static NOResult NO(Object data, String message) {
        return new NOResult().setData(data).setMessage(message);
    }

}